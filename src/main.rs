// Работа с файлами
// Запись файла

use std::fs::File;
use std::io::Write;
use std::io::Read;

extern crate telebot;
extern crate futures;
extern crate chrono;

use futures::stream::Stream;
use telebot::Bot;
use telebot::functions::*;
use chrono::{Datelike, Timelike, Utc};

#[allow(dead_code)]
fn file_write()
{
    let path = "data.txt";
    let mut f = File::create(path).expect("Error creating file");
    f.write_all("Test!".as_bytes()).expect("Error writing file");
    println!("File was written");
}

#[allow(dead_code)]
fn file_read()
{
    let path = "data.txt";
    let mut f = File::open(path).expect("Error open file");
    let mut file_data = String::new();

    f.read_to_string(&mut file_data).expect("Error read");
    println!("File data: {}", file_data);
}

#[allow(dead_code)]
fn print_time()
{
    let now = Utc::now();
    let (is_pm, hour) = now.hour12();
    println!(
        "The current UTC time is {:02}:{:02}:{:02} {}",
        hour,
        now.minute(),
        now.second(),
        if is_pm { "PM" } else { "AM" }
    );    

    let (is_common_era, year) = now.year_ce();
    println!(
        "The current UTC date is {}-{:02}-{:02} {:?} ({})",
        year,
        now.month(),
        now.day(),
        now.weekday(),
        if is_common_era { "CE" } else { "BCE" }
    );    
}

fn main() {

    let path = "phrases.txt";
    let mut f = File::open(path).expect("Ошибка открытия файла с фразами!");
    let mut file_data = String::new();
    let mut phrases: Vec<String> = Vec::new();
    f.read_to_string(&mut file_data).expect("Ошибка чтения файла!");
    
    for phrase in file_data.lines()
    {
        phrases.push(phrase.to_string());        
    }

    println!("В базе {} фраз(ы)!", phrases.len());    

    // Create the bot
    let mut bot = Bot::new("6373332058:AAGk8U2cOTGU8C-irZrjqbl_PxrU64TPaGU").update_interval(200);
    println!("Бот активирован!");

    // Register a reply command which answers a message
    let _handle_rply = bot.new_cmd("/bot")          
         .and_then(move |(bot, msg)| {

            let from_user = msg.text.unwrap().clone();


            // Выводим справочную информация по командам
            if from_user == "help"
            {
                let help_text = 
                "\"/bot joke\" - веселые пословицы и поговорки\n\"/bot another\" - еще какая-нибудь поебень!";
                bot.message(msg.chat.id, help_text.to_string()).send()                
            }

            // Выводим рандомную пословицу из базы
            else if from_user == "joke"
            {
                let rnd_index = (rand::random::<usize>()) % (phrases.len());
                let text = phrases[rnd_index].to_string();
                println!("-----------------------");
                print_time();
                println!("{:?}", msg.from.clone().unwrap().first_name);
                println!("Ответ: {text}");
                println!("-----------------------");
                bot.message(msg.chat.id, text).send()                 
            }

            else if from_user == "another"
            {                
                bot.message(msg.chat.id, "empty".to_string()).send()                
            }

            // Неизвестная команда
            else
            {
                bot.message(msg.chat.id, "Неизвестная команда".to_string()).send()
            }           
        })
        .for_each(|_| Ok(()));
    bot.run_with(_handle_rply);    
}



